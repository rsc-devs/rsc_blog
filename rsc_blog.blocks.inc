<?php


/**
 * Implements hook_block_info().
 */
function rsc_blog_block_info() {

  $blocks['menu'] = array(
    'info'  => t('RSC Blog: Blogs menu'),
    'cache' => DRUPAL_NO_CACHE,  // we cache the query result inside block_view
  );

  $blocks['latest'] = array(
    'info'  => t('RSC Blog: Latest blog posts'),
    'cache' => DRUPAL_NO_CACHE,  // we cache the query result inside block_view
  );

  $blocks['featured'] = array(
    'info'  => t('RSC Blog: Featured blog posts'),
    'cache' => DRUPAL_NO_CACHE,  // we cache the query result inside block_view
  );

  return $blocks;
}


/**
 * Implements hook_block_configure().
 */
function rsc_blog_block_configure($delta = '') {
  $form = array();
  switch ($delta) {
    case 'latest':
      $form['rsc_blog_num_latest'] = array(
        '#type' => 'numberfield',
        '#title' => t('Number of posts to display'),
        '#default_value' => variable_get('rsc_blog_num_latest', 5),
      );
      break;
    case 'featured':
      $form['rsc_blog_num_featured'] = array(
        '#type' => 'numberfield',
        '#title' => t('Number of posts to display'),
        '#default_value' => variable_get('rsc_blog_num_featured', 5),
      );
      break;
  }
  return $form;
}


/**
 * Implements hook_block_save().
 */
function rsc_blog_block_save($delta = '', $edit = array()) {
  switch ($delta) {
    case 'latest':
      variable_set('rsc_blog_num_latest', $edit['rsc_blog_num_latest']);
      cache_clear_all('rsc_blog_latest_block_cache', 'cache');
      break;
    case 'featured':
      variable_set('rsc_blog_num_featured', $edit['rsc_blog_num_featured']);
      cache_clear_all('rsc_blog_featured_block_cache', 'cache');
      break;
  }
}


/**
 * Implements hook_block_view().
 */
function rsc_blog_block_view($delta = '') {

  module_load_include('inc','rsc_blog','rsc_blog.common');

  $block = array();

  switch ($delta) {
    case 'menu':
      $block['subject'] = 'Blogs';
      $cached_content = cache_get('rsc_blog_menu_block_content');
      if (empty($cached_content)) {
        $block['content'] = _rsc_blog__menu_block_content();
        cache_set('rsc_blog_menu_block_content', $block['content'], 'cache', CACHE_PERMANENT);
      } else {
        $block['content'] = $cached_content->data;
      }
      break;
    case 'latest':
      $block['subject'] = 'Latest blog posts';
      $cached_content = cache_get('rsc_blog_latest_block_cache');
      if (empty($cached_content)) {
        $block['content'] = _rsc_blog__latest_block_content(variable_get('rsc_blog_num_latest', 5));
        cache_set('rsc_blog_latest_block_cache', $block['content'], 'cache', CACHE_PERMANENT);
      } else {
        $block['content'] = $cached_content->data;
      }
      break;
    case 'featured':
      $block['subject'] = 'Featured blog posts';
      $cached_content = cache_get('rsc_blog_featured_block_cache');
      if (empty($cached_content)) {
        $block['content'] = _rsc_blog__featured_block_content(variable_get('rsc_blog_num_featured', 5));
        cache_set('rsc_blog_featured_block_cache', $block['content'], 'cache', CACHE_PERMANENT);
      } else {
        $block['content'] = $cached_content->data;
      }
      break;
  }

  return $block;
}


function _rsc_blog__menu_block_content() {
  module_load_include('inc','rsc_blog','rsc_blog.common');
  $bloggers = load_bloggers();

  if (!count($bloggers)) {
    return array(
      '#markup' => t(
        'There are no bloggers on the site. To start a blog, give someone the !role role.',
        array('!role' => '<em>rsc blogger</em>')
      ),
    );
  }

  // Make a list of links to blogs
  $links = array();
  foreach ($bloggers as $rid => $blogger) {
    $links[] = l(get_blog_title($blogger), '/blog/'.$blogger->name);
  }

  return array(
    '#theme' => 'item_list',
    '#list_type' => 'ul',
    '#items' => $links,
  );

}


/**
 *
 */
function _rsc_blog__latest_block_content($num) {
  $posts = load_latest_blog_posts(NULL, NULL, $num);
  if (count($posts)) {
    return node_view_multiple($posts,'rscb_block');
  } else {
    return array('#markup' => t('There are no blog posts.'));
  }
}


/**
 *
 */
function _rsc_blog__featured_block_content($num) {
  $posts = load_featured_blog_posts(NULL, NULL, $num, TRUE);
  if (count($posts)) {
    return node_view_multiple($posts,'rscb_block');
  } else {
    return array('#markup' => t('There are no featured blog posts.'));
  }
}
