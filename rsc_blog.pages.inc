<?php


function rsc_blog_blogger_page($username) {
  module_load_include('inc','rsc_blog','rsc_blog.common');

  // TODO: cache page
  // TODO: trigger a cache clear when the user's profile is edited, or when they post new content.
  $user = user_load_by_name($username);

  // If the requested user is not a blogger, this page does not exist. Return 404.
  $rid = array_search('rsc blogger', user_roles());
  if (!user_has_role($rid, $user)) {
    return MENU_NOT_FOUND;
  }

  drupal_set_title(get_blog_title($user));
  $contents = array();

  $user_wrapper = entity_metadata_wrapper('user', $user);
  $rscu_fullname = $user_wrapper->rscu_fullname->value();
  $rscb_blogtitle = $user_wrapper->rscb_blogtitle->value();

  if (!empty($rscb_blogtitle)) {
    // The blog has a special title, which means that the user's name is not in the title.
    // So we add a subtitle to indicate the author.
    $contents['subtitle'] = array(
      // TODO container or prefix-suffix to make the subtitle themeable?
      '#markup' => t("By @name", array(
        '@name' => (empty($rscu_fullname)) ? format_username($user) : ($rscu_fullname)
      ))
    );
  }

  $contents['bio'] = user_view($user, 'rscb_bio');

  // Show a list of links to the latest posts
  $posts = load_latest_blog_posts($user, variable_get('rsc_blog_posts_per_page', 10));

  $num_posts = count($posts);
  if ($num_posts) {
    $contents['pager_top'] = array(
      '#theme' => 'pager'
    );
    $contents['posts'] = node_view_multiple($posts, 'teaser');
    $contents['pager_bottom'] = array(
      '#theme' => 'pager'
    );
  } else {
    $contents['posts'] = array(
      '#markup' => t("This blog has not yet published any posts."),
    );
  }



  return $contents;

}
