<?php


/**
 * (re)set content type settings
 */
function rsc_blog_content_type_settings() {

  $type = 'rsc_blog';

  // settings from the content type manage page
  variable_set("node_preview_{$type}", 1);         // Optional preview
  variable_set("node_options_{$type}", array());   // default unpublished
  variable_set("node_submitted_{$type}", 1);       // 'Display author and date information'.
  variable_set("comment_{$type}", 2);              // open comments. Don't use the COMMENT_NODE_OPEN constant, so that we don't have to depend on the comment module.
  variable_set("menu_options_{$type}", array());   // no available menus

  // pathauto settings
  variable_set("pathauto_node_rsc_blog_pattern", 'blog/[node:title]');

}


/**
 * (re)create fields
 */
function rsc_blog_field_config($tag_vid) {

  $fieldnames = array_keys(field_info_fields());
  $prefix = 'rscb'; // RSC Blog

  /* body text */
  $fieldname = "{$prefix}_body";
  if (!in_array($fieldname,$fieldnames)) {
    field_create_field(array(
      'field_name'  => $fieldname,
      'type'        => 'text_long',
      'cardinality' => 1,
    ));
  }

  /* Tags term reference */
  $tag_vocab = taxonomy_vocabulary_load($tag_vid);

  if (!empty($tag_vocab)) {                             // if a vocabulary has been configured
    $fieldname = "{$prefix}_tag_{$tag_vocab->machine_name}"; // determine what this field should be called
    if (strlen($fieldname) <= 32) {
      if (!in_array($fieldname, $fieldnames)) {          // create it if it doesn't exist yet
        field_create_field(array(
          'field_name'  => $fieldname,
          'type'        => 'taxonomy_term_reference',
          'cardinality' => FIELD_CARDINALITY_UNLIMITED,
          'settings'    => array(
            'allowed_values' => array(
              array(
                'vocabulary' => $tag_vocab->machine_name,
                'parent'     => '0',
              ),
            ),
          ),
        ));
        $fieldnames[] = $fieldname; // add field to list of names to prevent duplicate creation attempts
      }
    } else {
      drupal_set_message(t('Cannot create field @fieldname (more than 32 characters).', array('@fieldname' => $fieldname)),'error');
    }
  } else {
    drupal_set_message(t('Could not create fields for rsc_blog, because vocabulary settings have not been configured yet.'), 'error');
  }

  /* Blog title field */
  $fieldname = "{$prefix}_blogtitle";
  if (!in_array($fieldname,$fieldnames)) {
    field_create_field(array(
      'field_name'  => $fieldname,
      'type'        => 'text',
      'cardinality' => 1,
    ));
  }

}


/**
 * (re)configure field instances
 */
function rsc_blog_field_instance_config($tag_vid) {
  $t = get_t(); // this function might run during install, or any other time
  $prefix = "rscb";
  $fields = array_keys(field_info_fields());
  $instances_blog = array_keys(field_info_instances('node','rsc_blog'));
  $instances_user = array_keys(field_info_instances('user','user'));

  $tag_vocab = taxonomy_vocabulary_load($tag_vid);

  /* Tags term reference */
  if (!empty($tag_vocab)) {                                 // if tag vocabulary has been configured
    $fieldname = "{$prefix}_tag_{$tag_vocab->machine_name}"; // determine what this field is called
    if (in_array($fieldname, $fields)) {                     // if the field exists

      // rsc_blog:
      if (!in_array($fieldname, $instances_blog)) { // if the instance does not exist yet
        field_create_instance(array(               // create the instance
          'field_name'  => $fieldname,
          'entity_type' => 'node',
          'bundle'      => 'rsc_blog',
          'label'       => $t('Tags'),
          'required'    => false,
          'display'     => array(
            'default' => array(
              'type' => 'hidden',
            ),
            'teaser' => array(
              'type' => 'hidden',
            ),
            'rscb_block' => array(
              'type' => 'hidden',
            ),
          ),
          'widget'      => array('type' => 'options_select'),
        ));
        $instances_blog[] = $fieldname;
      }

    } else {
      drupal_set_message(t('Cannot create instances of non-existing field <strong>@fieldname</strong> on content type <strong>@type</strong>.',array('@fieldname'=>$fieldname,'@type'=>'rsc_blog')),'error');
    }
  }

  /* Body text */
  $fieldname = "{$prefix}_body";

  // rsc_blog:
  if (!in_array($fieldname, $instances_blog) && in_array($fieldname, $fields)) {
    field_create_instance(array(
      'field_name'  => $fieldname,
      'entity_type' => 'node',
      'bundle'      => 'rsc_blog',
      'label'       => $t('Body'),
      'required'    => false,
      'settings'    => array('text_processing' => 1), // allow text processing
      'display'     => array(
        'default' => array(
          'label'  => 'hidden',
          'type' => 'text_default',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'type' => 'text_trimmed',
          'settings' => array(
            'trim_length' => 300,
          ),
        ),
        'rscb_block' => array(
          'type' => 'hidden',
        ),
      ),
    ));
    $instances_blog[] = $fieldname;
  }

  /* Blog title field */
  $fieldname = "{$prefix}_blogtitle";

  // user:
  if (!in_array($fieldname, $instances_user) && in_array($fieldname, $fields)) {
    field_create_instance(array(
      'field_name'  => $fieldname,
      'entity_type' => 'user',
      'bundle'      => 'user',
      'label'       => $t('Blog title'),
      'required'    => false,
    ));
    $instances[] = $fieldname;
  }

}


/**
 * Delete unused fields that start with rscb_
 * Unused means: does not have any instances
 */
function rsc_blog_field_cleanup() {
  $fields = array_keys(field_info_fields());
  $used_fields = array_keys(field_info_field_map());
  $unused_fields = array_diff($fields,$used_fields);

  foreach ($unused_fields as $field) {
    if (substr($field,0,5) === 'rscb_') {
      field_delete_field($field);
    }
  }
}


/**
 * Load users that are bloggers
 */
function load_bloggers() {
  module_load_include('inc','rsc_user','rsc_user.common');
  return user_load_multiple(get_user_ids_having_roles(array(array_search('rsc blogger', user_roles()))));
}


/**
 * Work out the title for the user's blog
 */
function get_blog_title($user) {

  $user_wrapper = entity_metadata_wrapper('user', $user);

  // If the user set a explicit title for their blog, use that
  $rscb_blogtitle = $user_wrapper->rscb_blogtitle->value();
  if (!empty($rscb_blogtitle)) {
    return $rscb_blogtitle;
  }

  // Otherwise, use the user's full name
  $rscu_fullname = $user_wrapper->rscu_fullname->value();
  if (!empty($rscu_fullname)) {
    return t("@name's blog", array('@name' => $rscu_fullname));
  }

  // If the user doesn't didn't specify a full name, use the username
  return t("@name's blog", array('@name' => format_username($user)));

}


/**
 * Get the latest published blog posts, optionally filtered by blogger, optionally paginated, newest first.
 */
function get_latest_blog_post_nids($user = NULL, $num_per_page = NULL, $limit = NULL) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'rsc_blog')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->propertyOrderBy('created', 'DESC');

  if ($user) {
    // If the specified user is not a blogger, return an empty list.
    $rid = array_search('rsc blogger', user_roles());
    if (!user_has_role($rid, $user)) {
      return array();
    }

    // Otherwise filter by the specified user
    $query->propertyCondition('uid', $user->uid);
  }

  // The pager and the range are mutually exclusive.
  // Use the pager when getting nodes for showing in a page,
  // and the limit for using the nodes in a block.
  if (is_numeric($num_per_page)) {
    $query->pager($num_per_page);
  } elseif (is_numeric($limit)) {
    $query->range(0, $limit);
  }

  $result = $query->execute();
  if (!isset($result['node'])) {
    return array();
  }

  return array_keys($result['node']);
}


/**
 * Get the published featured blog posts, optionally filtered by blogger, optionally paginated, in no particular order.
 */
function get_featured_blog_post_nids($user = NULL, $num_per_page = NULL, $limit = NULL) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'rsc_blog')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->propertyCondition('promote', NODE_PROMOTED);

  if ($user) {
    // If the specified user is not a blogger, return an empty list.
    $rid = array_search('rsc blogger', user_roles());
    if (!user_has_role($rid, $user)) {
      return array();
    }

    // Otherwise filter by the specified user
    $query->propertyCondition('uid', $user->uid);
  }

  // The pager and the range are mutually exclusive.
  // Use the pager when getting nodes for showing in a page,
  // and the limit for using the nodes in a block.
  if (is_numeric($num_per_page)) {
    $query->pager($num_per_page);
  } elseif (is_numeric($limit)) {
    $query->range(0, $limit);
  }

  $result = $query->execute();
  if (!isset($result['node'])) {
    return array();
  }

  return array_keys($result['node']);
}


/**
 * Load the latest published blog posts, optionally filtered by blogger, optionally paginated, newest first.
 */
function load_latest_blog_posts($user = NULL, $num_per_page = NULL, $limit = NULL) {
  return node_load_multiple(get_latest_blog_post_nids($user, $num_per_page, $limit));
}


/**
 * Load the published featured blog posts, optionally filtered by blogger, optionally paginated,
 * in no particular order or shuffled.
 */
function load_featured_blog_posts($user = NULL, $num_per_page = NULL, $limit = NULL, $shuffle = FALSE) {
  $nids = get_featured_blog_post_nids($user, $num_per_page, $limit);
  if ($shuffle) {
    shuffle($nids);
  }
  return node_load_multiple($nids);
}
